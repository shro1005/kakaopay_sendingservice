package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dao.SendingRepository;
import com.kyk.preprob.kakaopay.demo.dto.RequestSendDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;
import com.kyk.preprob.kakaopay.demo.util.exception.DividedByZeroException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service("SendingService")
public class SendingServiceImpl implements SendingSerivce {
    @Autowired
    SendingRepository sendingRepository;

    @Override
    public Sending insertSending(Long userId, String roomId, RequestSendDto requestSendDto) throws Exception {
        log.debug("요청ID : {} / 요청 방ID : {} / parma : {}", userId, roomId, requestSendDto.toString());
        int totalAmt = requestSendDto.getTotalAmt();
        int divCnt = requestSendDto.getDivisionCnt();
        if (divCnt == 0) {
            throw new DividedByZeroException("뿌릴 대상 인원을 0명으로 할 수 없습니다.");
        }

        Sending sending = Sending.builder()
                .userId(userId)
                .roomId(roomId)
                .totalAmt(totalAmt)
                .divisionCnt(divCnt)
                .build();
        sending = sendingRepository.save(sending);
        log.info("sending Table insert SUCCESS | {}", sending.toString());

        return sending;
    }
}
