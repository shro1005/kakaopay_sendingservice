package com.kyk.preprob.kakaopay.demo.controller;

import com.kyk.preprob.kakaopay.demo.dto.ErrResponseDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSearchDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSendDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseTakeDto;
import com.kyk.preprob.kakaopay.demo.util.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.nio.file.AccessDeniedException;

@Slf4j
@ControllerAdvice
public class ExceptionController {
    // send
    @ExceptionHandler({DividedByZeroException.class})
    public ResponseEntity<ResponseSendDto> SendException(Exception e) {
        log.info(e.getMessage());
        ResponseSendDto responseDto = ResponseSendDto.builder()
                .responseCd(400)
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    // take
    @ExceptionHandler({TakeMoneyWarning.class
                    , TakeMoneyException.class})
    public ResponseEntity<ResponseTakeDto> TakeException(Exception e) {
        log.info(e.getMessage());
        ResponseTakeDto responseDto = ResponseTakeDto.builder()
                .responseCd(400)
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    // search
    @ExceptionHandler({TokenExpireException.class
                     , SearchSendingException.class})
    public ResponseEntity<ResponseSearchDto> SearchException(Exception e) {
        log.info(e.getMessage());
        ResponseSearchDto responseDto = ResponseSearchDto.builder()
                .responseCd(400)
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }

    // 400
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<ErrResponseDto> BadRequestException(Exception e) {
        log.warn("400 error 발생 | {}", e);
        ErrResponseDto errResponseDto = ErrResponseDto.builder()
                .responseCd(400)
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(errResponseDto, HttpStatus.BAD_REQUEST);
    }

    // 401
    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<ErrResponseDto> handleAccessDeniedException(Exception e) {
        log.warn("400 error 발생 | {}", e);
        ErrResponseDto errResponseDto = ErrResponseDto.builder()
                .responseCd(401)
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(errResponseDto, HttpStatus.UNAUTHORIZED);
    }

    // 500
    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrResponseDto> handleAll(Exception e) {
        log.info(e.getClass().getName());
        log.error("500 error 발생 | {}", e);
        ErrResponseDto errResponseDto = ErrResponseDto.builder()
                .responseCd(500)
                .message(e.getMessage())
                .build();
        return new ResponseEntity<>(errResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
