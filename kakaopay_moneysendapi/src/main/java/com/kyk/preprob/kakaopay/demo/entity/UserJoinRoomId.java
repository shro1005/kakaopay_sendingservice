package com.kyk.preprob.kakaopay.demo.entity;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class UserJoinRoomId implements Serializable {

//    @EqualsAndHashCode.Include
//    @Id
//    @Column(name = "join_id")
//    private Long joinId;

    @EqualsAndHashCode.Include
    @Id
    @Column(name = "user_id")
    private Long userId;

    @EqualsAndHashCode.Include
    @Id
    @Column(name = "room_id")
    private String roomId;

}
