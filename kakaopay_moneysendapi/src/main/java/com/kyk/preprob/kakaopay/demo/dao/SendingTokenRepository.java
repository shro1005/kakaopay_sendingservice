package com.kyk.preprob.kakaopay.demo.dao;

import com.kyk.preprob.kakaopay.demo.entity.SendingToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;

public interface SendingTokenRepository extends JpaRepository<SendingToken, String> {
    Long countByToken(String token);

    SendingToken findSendingTokenByTokenAndExpireDtmIsGreaterThan(String token, LocalDateTime currentDtime);

    SendingToken findSendingTokenByTokenAndSendingRegistDtimeGreaterThanEqual(String token, LocalDateTime lastWeekDtime);

    SendingToken findByToken(String token);
}
