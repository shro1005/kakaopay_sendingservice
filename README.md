문제 해결 전략

1. 토큰 발급의 경우 무직위 한글 3자를 합친 토큰을 발급합니다.   
   토큰은 sending_token 테이블에 키로 들어가며 뿌리기 id와 함께 저장됩니다.  
   토큰은 발급시 DB에 중복여부를 체크하며 발급됩니다.  
   토큰 조회가 가능한 7일이 지나면 자동으로 삭제하여 발급시 중복되는 경우를 최소한을 줄였습니다.  
    (경우의 수 ( 자음 19 * 모음 21 * 받침 28)^3 = 약 1조 3900 억)

2. 금액 분배의 경우 금액을 인원으로 나눈뒤 나머지가 존재할 시 최초 수령자에게 지급되도록 설정했습니다.

3. 받기, 조회 API의 검증의 경우 검증 서비스 (CheckRequestService)에서 검증하며 실패 응답을 내립니다.

4. 단위 테스트는 Junit5를 이용했으며, Controller, Service, DAO 의 단위테스트를 진행했습니다.  
   DAO 테스트시 인 메모리 h2를 이용하여 테스트를 진행하도록 설정했습니다. 

5. 다량의 트래픽 처리를 위해 Spring-Cloud-Ribbon을 이용한 별도의 로드밸런서 서버를 만들었으며  
    API서버 역시 스프링 부트에서 지원하는 멀티스래드로 트래픽 처리를 할 수 있습니다.  
    부하 테스트의 경우 Jmeter를 이용했습니다.


서버 기동 방법

1. PostgreSQL을 로컬 혹은 도커에 Run합니다. (포트, 유저, 패스워드, DB의 경우 아래와 동일하게 해주시면 됩니다.)  
 ex) docker run -p 5432:5432 -e POSTGRES_USER=kakao -e POSTGRES_PASSWORD=pay -e POSTGRES_DB=sending --name sending -d postgres

2. API서버를 기동합니다. (로드밸런서에 매핑된 포트 : 8444, 8445, 8446)  
 ex) cd kakaopay_moneysendapi 
     SERVER_PORT=8444 ./gradlew bootrun  (8445, 8446 동일)

3. 로드밸런서 서버를 기동합니다.  
 ex) cd loadbalancer_server 
     SERVER_PORT=8443 ./gradlew bootrun 

4. 포스트맨 등으로 테스트를 진행합니다.
    -  보내기 (POST)  
    url : /pay/sending/send  
    헤더 : X-USER-ID : 1 ~ 4 (DB 초기값) / X-ROOM-ID : "room1" ~ "room4" (초기값) / Content-Type : application/json  
    바디 : totalAmt (뿌릴 금액) / divisionCnt (나눌 인원)

    - 받기 (POST)
    url : /pay/sending/take  
    헤더 : X-USER-ID : 1 ~ 4 (DB 초기값) / X-ROOM-ID : "room1" ~ "room4" (초기값) / Content-Type : application/json  
    바디 : token (토큰)

    - 조회 (GET)
    url : /pay/sending/search?token={토큰값}   
    헤더 : X-USER-ID : 1 ~ 4 (DB 초기값) / X-ROOM-ID : "room1" ~ "room4" (초기값) 
     
