package com.kyk.preprob.kakaopay.demo.entity;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public class SendingDetailId implements Serializable {
    @EqualsAndHashCode.Include
    @Id
    @Column(name = "sending_id")
    private Long sendingId;

//    @EqualsAndHashCode.Include
//    @Id
//    @Column(name = "user_id")
//    private Long userId;
//
//    @EqualsAndHashCode.Include
//    @Id
//    @Column(name = "room_id")
//    private String roomId;

    @EqualsAndHashCode.Include
    @Id
    @Column(name = "s_order")
    private int order;

}
