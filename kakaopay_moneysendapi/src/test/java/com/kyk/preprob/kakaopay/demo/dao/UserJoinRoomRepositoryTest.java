package com.kyk.preprob.kakaopay.demo.dao;

import com.kyk.preprob.kakaopay.demo.entity.UserJoinRoom;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
@ActiveProfiles("test-db")
class UserJoinRoomRepositoryTest {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private UserJoinRoomRepository joinRoomRepository;

    @Test
    void 해당_유저_채팅방_참여여부_Y() {
        //given
        UserJoinRoom userJoinRoom = new UserJoinRoom(1l, "방1", "Y");

        entityManager.persist(userJoinRoom);

        //when
        UserJoinRoom result = joinRoomRepository.findByUserIdAndRoomIdAndAndJoinYn(1l, "방1", "Y");

        //then
        assertEquals(1l, result.getUserId());
        assertEquals("방1", result.getRoomId());
        assertEquals("Y", result.getJoinYn());
    }

    @Test
    void 해당_유저_채팅방_참여여부_N() {
        //given
        UserJoinRoom userJoinRoom = new UserJoinRoom(1l, "방222", "Y");

        entityManager.persist(userJoinRoom);

        //when
        UserJoinRoom result = joinRoomRepository.findByUserIdAndRoomIdAndAndJoinYn(1l, "방1", "Y");

        //then
        assertEquals(null, result);
    }
}