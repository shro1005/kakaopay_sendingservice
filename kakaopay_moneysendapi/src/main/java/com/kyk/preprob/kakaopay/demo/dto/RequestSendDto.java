package com.kyk.preprob.kakaopay.demo.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RequestSendDto {
    private int totalAmt;
    private int divisionCnt;
}
