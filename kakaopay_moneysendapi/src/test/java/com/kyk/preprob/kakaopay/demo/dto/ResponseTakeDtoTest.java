package com.kyk.preprob.kakaopay.demo.dto;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ResponseTakeDtoTest {
    @Test
    void builder_and_getter_test() {
        //given
        final ResponseTakeDto responseTakeDto = ResponseTakeDto.builder()
                .responseCd(200)
                .message("test")
                .receiveAmt(1000)
                .build();

        //when
        final int responseCd = responseTakeDto.getResponseCd();
        final String message = responseTakeDto.getMessage();
        final int receiveAmt = responseTakeDto.getReceiveAmt();

        //then
        assertThat(responseCd).isEqualTo(200);
        assertThat(message).isEqualTo("test");
        assertThat(receiveAmt).isEqualTo(1000);
    }
}