package com.kyk.preprob.kakaopay.demo.util.exception;

public class TokenExpireException extends Exception {
    public TokenExpireException(String msg) {
        super(msg);
    }
}
