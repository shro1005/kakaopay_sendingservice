package com.kyk.preprob.kakaopay.demo.service;

import com.kyk.preprob.kakaopay.demo.dto.RequestSendDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseSearchDto;
import com.kyk.preprob.kakaopay.demo.dto.ResponseTakeDto;
import com.kyk.preprob.kakaopay.demo.entity.Sending;

public interface SendingDetailService {
    void insertSendingDetails(Sending sending) throws Exception;

    ResponseTakeDto updateReceivedSendingDetail(Long userId, Sending sending) throws Exception;

    ResponseSearchDto findDetailBySending(Sending sending) throws Exception;
}
