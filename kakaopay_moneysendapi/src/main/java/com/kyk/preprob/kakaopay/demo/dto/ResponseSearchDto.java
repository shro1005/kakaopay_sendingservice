package com.kyk.preprob.kakaopay.demo.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
public class ResponseSearchDto {
    private int responseCd;
    private String message;
    private LocalDateTime sendingTime;
    private int totalAmt;
    private int totalReceivedAmt;
    private List<TakeFinishedInfoDto> receiveFinishedInfoList;

    public ResponseSearchDto(int responseCd, String message, LocalDateTime sendingTime,
                             int totalAmt, int totalReceivedAmt, List<TakeFinishedInfoDto> receiveFinishedInfoList) {
        this.responseCd = responseCd;
        this.message = message;
        this.sendingTime = sendingTime;
        this.totalAmt = totalAmt;
        this.totalReceivedAmt = totalReceivedAmt;
        this.receiveFinishedInfoList = receiveFinishedInfoList;
    }
}
