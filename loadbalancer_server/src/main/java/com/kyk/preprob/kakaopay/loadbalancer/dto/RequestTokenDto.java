package com.kyk.preprob.kakaopay.loadbalancer.dto;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RequestTokenDto {
    private String token;
}
