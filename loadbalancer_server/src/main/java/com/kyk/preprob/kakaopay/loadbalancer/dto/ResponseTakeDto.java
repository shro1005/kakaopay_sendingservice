package com.kyk.preprob.kakaopay.loadbalancer.dto;

import lombok.*;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
public class ResponseTakeDto {
    private int responseCd;
    private String message;
    private int receiveAmt;

    public ResponseTakeDto(int responseCd, String message, int receiveAmt) {
        this.responseCd = responseCd;
        this.message = message;
        this.receiveAmt = receiveAmt;
    }
}
