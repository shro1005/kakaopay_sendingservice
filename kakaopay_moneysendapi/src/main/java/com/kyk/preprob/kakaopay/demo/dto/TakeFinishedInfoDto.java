package com.kyk.preprob.kakaopay.demo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.stereotype.Service;

@Getter
@Service
@ToString
@NoArgsConstructor
public class TakeFinishedInfoDto {
    private int receiveAmt;
    private Long receiveUserId;

    public TakeFinishedInfoDto(int receiveAmt, Long receiveUserId) {
        this.receiveAmt = receiveAmt;
        this.receiveUserId = receiveUserId;
    }
}
