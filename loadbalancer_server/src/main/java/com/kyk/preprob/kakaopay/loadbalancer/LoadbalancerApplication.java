package com.kyk.preprob.kakaopay.loadbalancer;

import com.kyk.preprob.kakaopay.loadbalancer.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootApplication
@RestController
@RibbonClient(name = "sending-server", configuration = LoadbalanceConfig.class)
public class LoadbalancerApplication {
    private static final String SERVER_URL = "http://sending-server/pay/sending/";

    @Bean
    @LoadBalanced
    RestTemplate ribbonRestTemplate(){
        return new RestTemplate();
    }

    @LoadBalanced
    @Autowired
    private RestTemplate ribbonRestTemplate;

    @RequestMapping("/")
    public String healthCheck() {
        String greeting = ribbonRestTemplate.getForObject("http://sending-server/", String.class);
        log.info(greeting);
        return greeting;
    }

    @RequestMapping(value = "/pay/sending/send", method = RequestMethod.POST)
    public ResponseEntity<ResponseSendDto> send(@RequestHeader("X-USER-ID") Long userId,
                                               @RequestHeader("X-ROOM-ID") String roomId,
                                               @RequestBody RequestSendDto requestSendDto) {
        //header 생성
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-USER-ID", String.valueOf(userId));
        headers.set("X-ROOM-ID", roomId);

        //request body 생성
        Map<String, Object> map = new HashMap<>();
        map.put("totalAmt", requestSendDto.getTotalAmt());
        map.put("divisionCnt", requestSendDto.getDivisionCnt());

        //builde header
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        ResponseEntity<ResponseSendDto> responseSendDto;
        //서버로 요청
        try {
            responseSendDto = ribbonRestTemplate.postForEntity(SERVER_URL + "send", entity, ResponseSendDto.class);
        }catch (Exception e) {
            ResponseSendDto errDto = ResponseSendDto.builder()
                    .responseCd(500)
                    .message("서버와의 연결이 끊겼습니다. 잠시만 기다려주세요.")
                    .build();
            responseSendDto = new ResponseEntity<>(errDto, HttpStatus.INTERNAL_SERVER_ERROR);
            e.getMessage();
        }
        return responseSendDto;
    }

    @RequestMapping(value = "/pay/sending/take", method = RequestMethod.POST)
    public ResponseEntity<ResponseTakeDto> take(@RequestHeader("X-USER-ID") Long userId,
                                                @RequestHeader("X-ROOM-ID") String roomId,
                                                @RequestBody RequestTokenDto requestTokenDto) {
        //header 생성
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-USER-ID", String.valueOf(userId));
        headers.set("X-ROOM-ID", roomId);

        //request body 생성
        Map<String, Object> map = new HashMap<>();
        map.put("token", requestTokenDto.getToken());

        //builde header
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);

        ResponseEntity<ResponseTakeDto> responseTakeDto;
        //서버로 요청
        try {
            responseTakeDto = ribbonRestTemplate.postForEntity(SERVER_URL + "take", entity, ResponseTakeDto.class);
        }catch (Exception e) {
            ResponseTakeDto errDto = ResponseTakeDto.builder()
                    .responseCd(500)
                    .message("서버와의 연결이 끊겼습니다. 잠시만 기다려주세요.")
                    .build();
            responseTakeDto = new ResponseEntity<>(errDto, HttpStatus.INTERNAL_SERVER_ERROR);
            e.getMessage();
        }
        return responseTakeDto;
    }

    @RequestMapping(value = "/pay/sending/search", method = RequestMethod.GET)
    public ResponseEntity<ResponseSearchDto> search(@RequestHeader("X-USER-ID") Long userId,
                                                  @RequestHeader("X-ROOM-ID") String roomId,
                                                  @RequestParam(value = "token") String token) {
        //header 생성
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("X-USER-ID", String.valueOf(userId));
        headers.set("X-ROOM-ID", roomId);

        //builde header
        HttpEntity entity = new HttpEntity(headers);

        ResponseEntity<ResponseSearchDto> responseSearchDto;
        //서버로 요청
        try {
            responseSearchDto = ribbonRestTemplate.exchange(SERVER_URL + "search?token=" + token,
                    HttpMethod.GET, entity, ResponseSearchDto.class);
        }catch (Exception e) {
            ResponseSearchDto errDto = ResponseSearchDto.builder()
                    .responseCd(500)
                    .message("서버와의 연결이 끊겼습니다. 잠시만 기다려주세요.")
                    .build();
            responseSearchDto = new ResponseEntity<>(errDto, HttpStatus.INTERNAL_SERVER_ERROR);
            e.getMessage();
        }
        return responseSearchDto;
    }

    public static void main(String[] args) {
        SpringApplication.run(LoadbalancerApplication.class, args);
    }

}
