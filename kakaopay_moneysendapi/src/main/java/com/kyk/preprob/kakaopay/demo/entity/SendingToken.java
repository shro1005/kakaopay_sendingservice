package com.kyk.preprob.kakaopay.demo.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@ToString(exclude = "sending")
@Entity
@Table(name = "sending_token")
@IdClass(SendingId.class)
@NoArgsConstructor
public class SendingToken {
    @Id
    @Column(name = "token", nullable = false)
    private String token;

    @Column(name = "sending_id", nullable = false)
    private Long sendingId;

//    @Column(name = "is_expire", nullable = false)
//    private String isExpire;

    @Column(name = "expire_dtm")
    private LocalDateTime expireDtm;

    @Column(name = "sending_rgt_dtm")
    private LocalDateTime sendingRegistDtime;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    /** 연관관계만 맺는 역할만 하고 실제 값은 @id 컬럼을 실제 매핑에 사용 */
    @JoinColumns(value = {
            @JoinColumn(name = "sending_id", updatable = false, insertable = false)
//           ,@JoinColumn(name = "user_id", updatable = false, insertable = false)
//           ,@JoinColumn(name = "room_id", updatable = false, insertable = false)
    }, foreignKey = @ForeignKey(value = ConstraintMode.NO_CONSTRAINT))
    private Sending sending;

    public SendingToken(String token, Long sendingId, LocalDateTime expireDtm,
                        LocalDateTime sendingRegistDtime, Sending sending) {
        this.sendingId = sendingId;
        this.token = token;
//        this.isExpire = isExpire;
        this.expireDtm = expireDtm;
        this.sendingRegistDtime = sendingRegistDtime;
        this.sending = sending;
    }
}
